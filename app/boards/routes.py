from flask import Blueprint
from flask import Flask, render_template, request, redirect, url_for
from app.repos.boards import BoardsRepo
from app.repos.posts import PostsRepo
from app.main.routes import auth

boardsBP = Blueprint('boards', __name__)

#board route for listing all boards
@boardsBP.route("/boards")
def boards():
	auth()
	br = BoardsRepo()
	boards = BoardsRepo().get_all()
	#list comprehension to just get the board names ready to be used in the dropdown
	boardList = [x["board_id"] for x in BoardsRepo().get_all()]
	return render_template("boards.html", boards=boards, boardList=boardList)

#route for board page showing all posts related to that board
@boardsBP.route("/boards/<board_id>")
def board(board_id=None):
	auth()
	posts = PostsRepo().get_by_board(board_id)
	board = BoardsRepo().get_by_id(board_id)
	return render_template("board.html", posts=posts, board=board)

#route for new board creation page
@boardsBP.route("/boards/newboard", methods=["GET", "POST"])
def newboard():
	auth()
	if request.method == 'POST':
		board_id = request.form['board-title']
		board_summary = request.form['board-summary']
		new_board = BoardsRepo().create(board_id, board_summary)
		#url_for includes blueprint of which function you are calling from e.g. boards.boards
		return redirect(url_for('boards.boards'))
	return render_template("newboard.html")