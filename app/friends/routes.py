from flask import Blueprint
from flask import Flask, render_template, redirect, url_for, g, request
from app.repos.friend import FriendRepo
from app.repos.person import PersonRepo
from app.main.routes import auth



friendsBP = Blueprint('friends', __name__)

@friendsBP.route("/friends")
def friends():
	auth()
	friends = FriendRepo().get_by_person(g.id)
	requests = FriendRepo().get_friend_request(g.id)
	sent_requests = FriendRepo().get_sent_request(g.id)
	return render_template("friends.html", friends=friends, sent_requests=sent_requests, requests=requests)

@friendsBP.route("/friends/add", methods=['GET', 'POST'])
def addfriend():
	auth()
	if request.method == 'POST':
		username = request.form['friend-username']
		get_friend_id = PersonRepo().get_id_by_username(username)
		friend_id = get_friend_id["person_id"]
		if FriendRepo().exists(g.id, friend_id):
			return redirect(url_for("friends.friends"))
		else:
			send_request = FriendRepo().send_friend_request(g.id, friend_id)
			return redirect(url_for("friends.friends"))

	return render_template("addfriend.html")


@friendsBP.route("/friend/request/accept/<sender_username>/<person_id>")
def acceptFriend(sender_username=None, person_id=None):
	auth()
	sender = PersonRepo().get_id_by_username(sender_username)
	sender_id = sender['person_id']
	accept1 = FriendRepo().accept_request_1(g.id, sender_id)
	accept2 = FriendRepo().accept_request_2(sender_id, g.id)
	return redirect(url_for("friends.friends"))