from flask import Blueprint
from flask import Flask, render_template, redirect, url_for, g, request
from app.repos.comments import CommentsRepo
from app.main.routes import auth


commentsBP = Blueprint('comments', __name__)

@commentsBP.route("/comment/<comment_id>")
#why does this require None to be set?
def comment(comment_id=None):
	auth()
	comment = CommentsRepo().get_by_comment_id(comment_id)
	return render_template("comment.html", comment=comment)

@commentsBP.route("/comment/edit/<comment_id>", methods=["GET", "POST"])
def edit_comment(comment_id=None):
	auth()
	comment = CommentsRepo().get_by_comment_id(comment_id)
	post_id = (comment['post_id'])
	if request.method == 'GET':
		return render_template("editcomment.html", comment=comment)

	if request.method == 'POST':
		comment_body = request.form['edit-comment']
		update_comment = CommentsRepo().update_by_id(comment_body, comment_id, g.id)
		return redirect(url_for('posts.post', post_id=post_id))

	return render_template("home.html", error='Invalid post')

@commentsBP.route("/comment/remove/<comment_id>" ,methods=["GET"])
def remove_comment(comment_id=None):
	auth()
	comment = CommentsRepo().get_by_comment_id(comment_id)
	post_id = comment['post_id']
	removeComment = CommentsRepo().remove_by_id(comment_id, g.id)
	return redirect(url_for('posts.post', post_id=post_id))