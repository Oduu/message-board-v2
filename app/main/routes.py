from flask import Blueprint
from flask import Flask, render_template, request, redirect, url_for, abort, session, g
from app.repos.posts import PostsRepo

main = Blueprint('main', __name__)

@main.route("/", methods=['GET', 'POST'])
def home():
	auth()
	posts = PostsRepo().get_last_five_person(g.id)
	return render_template("home.html", posts=posts)

@main.route("/about")
def about():
	auth()
	return render_template("about.html")

#when using blueprints use before_app_request instead of before_request
#using before_request will only effect routes in this file where as before_app_request works across all blueprints
@main.before_app_request
def before_request():
	g.user = None
	if 'user' in session:
	 	g.user = session['user']
	 	g.id = session['person_id']

#check session
def auth():
	if g.user is None:
		abort(redirect(url_for("users.login")))