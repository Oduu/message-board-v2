from flask import Blueprint
from flask import Flask, render_template, request, redirect, url_for, g
from app.repos.posts import PostsRepo
from app.repos.comments import CommentsRepo
from app.repos.boards import BoardsRepo
from app.main.routes import auth

posts = Blueprint('posts', __name__)

@posts.route("/post/<post_id>" , methods=["GET", "POST"])
def post(post_id=None):
	auth()

	if request.method == 'GET':
		if post_id is not None:
			post = PostsRepo().get_by_id(post_id)
			comments = CommentsRepo().get_by_id(post_id)
			return render_template("post.html", post=post, comments=comments)

	if request.method == 'POST':
		comment_body = request.form['new-comment']
		new_comment = CommentsRepo().create(comment_body, post_id, g.id)
		return redirect(url_for('posts.post', post_id=post_id))

	return render_template("home.html", error='Invalid Post')

@posts.route("/post/edit/<post_id>", methods=["GET", "POST"])
def edit_post(post_id=None):
	auth()
	if request.method == 'GET':
		post = PostsRepo().get_by_id(post_id)
		return render_template("editpost.html", post=post)

	if request.method == 'POST':
		title = request.form['post-title']
		body = request.form['post-body']
		update_post = PostsRepo().update_by_id(post_id, title, body, g.id)
		return redirect(url_for('posts.post', post_id=post_id))

	return render_template("home.html", error='Invalid post')


@posts.route("/newpost", methods=["GET", "POST"])
def new_post():
	auth()
	#list comprehension to get board name/id ready for board selection
	boardList = [x["board_id"] for x in BoardsRepo().get_all()]
	if request.method == 'POST':
		title = request.form['post-title']
		body = request.form['post-body']
		board_id = request.form['board-select']
		new_post = PostsRepo().create(g.id, board_id, title, body)
		#url_for includes blueprint name e.g. main
		return redirect(url_for('main.home'))

	return render_template("newpost.html", boardList=boardList)

@posts.route("/myposts")
def my_posts():
	auth()
	posts = PostsRepo().get_by_person_ordered(g.id)
	return render_template("myposts.html", posts=posts)

@posts.route("/post/remove/<post_id>")
def remove_post(post_id=None):
	auth()
	removePost = PostsRepo().remove_by_id(post_id, g.id)
	return redirect(url_for('main.home'))

@posts.route("/post/archive/<post_id>")
def archive_post(post_id=None):
	auth()
	archivePost = PostsRepo().archive_by_id(post_id, g.id)
	return redirect(url_for('posts.post', post_id=post_id))