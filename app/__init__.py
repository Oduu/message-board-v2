from flask import Flask

app = Flask(__name__)

#to restrict users from changing their own cookies to be another user
app.secret_key = b'\xebkY\x8e\xb7\xf5\xa3\xbe\xfa(|M\x186\x12v\xba\x04aGuEX\xd0'

#from file import blueprint
from app.users.routes import users
from app.boards.routes import boardsBP
from app.friends.routes import friendsBP
from app.posts.routes import posts
from app.main.routes import main
from app.comments.routes import commentsBP

#activate blueprints
app.register_blueprint(users)
app.register_blueprint(boardsBP)
app.register_blueprint(friendsBP)
app.register_blueprint(posts)
app.register_blueprint(main)
app.register_blueprint(commentsBP)