#this file contains all of the database boards table related functions
from app.repos.conn import DB

class BoardsRepo:
	def get_all(self):
		DB.cursor.execute(""" SELECT board_id FROM boards """)
		return DB.cursor.fetchall()

	def get_by_id(self, board_id):
		DB.cursor.execute(""" SELECT * FROM boards WHERE board_id = %s """ \
			,(board_id,))
		return DB.cursor.fetchone()

	def create(self, board_id, board_summary):
		DB.cursor.execute(""" INSERT INTO boards (board_id, summary) VALUES (%s, %s) """ \
			,(board_id, board_summary))
		DB.connection.commit();

