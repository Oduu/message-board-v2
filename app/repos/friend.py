#this file contains all of the database friend table related functions
from app.repos.conn import DB

class FriendRepo:
	def get_by_person(self,person_id):
		DB.cursor.execute(""" SELECT P.username FROM friend AS F INNER JOIN person as P on P.person_id = F.friend_id where F.person_id = %s AND accepted = 1 """ \
			,(person_id,))
		return DB.cursor.fetchall();

	def get_by_person_union(self, person_id):
		DB.cursor.execute(""" SELECT P.username FROM friend AS F INNER JOIN person as P ON P.person_id = F.person_id WHERE friend_id = %s
		UNION 
		SELECT P.username FROM friend AS F INNER JOIN person as P ON P.person_id = F.friend_id WHERE F.person_id = %s """ \
			,(person_id, person_id))
		return DB.cursor.fetchall();

	def send_friend_request(self, person_id, friend_id):
		DB.cursor.execute(""" INSERT INTO friend(person_id, friend_id, accepted) VALUES(%s, %s, 0) """ \
			,(person_id, friend_id))
		DB.connection.commit();

	#not used just for reference
	def send_many(self, person_id, friend_id):
		DB.cursor.execute(""" INSERT INTO friend(person_id, friend_id, accepted) VALUES(%s, %s, 0), VALUES(%s, %s, 0) """ \
			,(person_id, friend_id, friend_id, person_id))
		DB.connection.commit();

	def get_sent_request(self, person_id):
		DB.cursor.execute(""" SELECT P.username FROM friend AS F INNER JOIN person as P on P.person_id = F.friend_id where F.person_id = %s AND accepted = 0 """ \
			,(person_id,))
		return DB.cursor.fetchall();

	def get_friend_request(self, person_id):
		DB.cursor.execute(""" SELECT P.username FROM friend AS F INNER JOIN person as P on P.person_id = F.person_id where F.friend_id = %s AND accepted = 0 """ \
			,(person_id,))
		return DB.cursor.fetchall();

	def accept_request_1(self, person_id, friend_id):
		DB.cursor.execute(""" INSERT INTO friend(person_id, friend_id, accepted) VALUES(%s, %s, 1) """ \
			,(person_id, friend_id))
		DB.connection.commit();

	def accept_request_2(self, friend_id, person_id):
		DB.cursor.execute(""" UPDATE friend SET accepted = 1 WHERE person_id = %s AND friend_id = %s """ \
			,(friend_id, person_id))
		DB.connection.commit();

	def exists(self, person_id, friend_id):
		DB.cursor.execute(""" SELECT count(*) 'count' FROM friend WHERE person_id = %s AND friend_id = %s or person_id = %s AND friend_id = %s """ \
			,(person_id, friend_id, friend_id, person_id))
		return DB.cursor.fetchone()['count'] > 0