#this file contains all of the database comments table related functions
from app.repos.conn import DB

class CommentsRepo:
	def get_by_id(self, post_id):
		DB.cursor.execute(""" SELECT C.*, P.username FROM comments as C INNER JOIN person as P ON P.person_id = C.person_id WHERE C.post_id = %s """ \
		,(post_id,))
		return DB.cursor.fetchall()

	def create(self, body, post_id, person_id):
		DB.cursor.execute(""" INSERT INTO comments(body, post_id, person_id) values(%s, %s, %s)""" \
						,(body, post_id, person_id))
		DB.connection.commit();

	def get_by_comment_id(self, comment_id):
		DB.cursor.execute(""" SELECT * FROM comments WHERE comment_id = %s """ \
			,(comment_id))
		return DB.cursor.fetchone()

	def update_by_id(self, body, comment_id, person_id):
		DB.cursor.execute(""" UPDATE comments SET body = %s WHERE comment_id = %s AND person_id = %s """ \
		,( body, comment_id, person_id))
		DB.connection.commit();

	def remove_by_id(self, comment_id, person_id):
		DB.cursor.execute(""" DELETE FROM comments WHERE comment_id = %s AND person_id = %s """ \
			,(comment_id, person_id))
		DB.connection.commit();