#this file contains all of the database person table related functions
from app.repos.conn import DB

class PersonRepo:
	#exists function checks to see if the username chosen by the user already exists within the database by seeing if there is a row returned by the select query
	def exists(self, name):
		DB.cursor.execute(""" SELECT count(*) 'count' FROM person WHERE username = %s """ \
			,(name,))
		#singluar variables being passed through to SQL within the parameter need to have a comma at the end to declare it as a tuple
		return DB.cursor.fetchone()['count'] > 0

	def create(self, name, password, email):
		DB.cursor.execute(""" INSERT into person(username, password, email) values(%s, %s, %s)""" \
						,(name, password ,email))
		DB.connection.commit();

	def get_by_username(self, username):
		DB.cursor.execute(""" SELECT person_id, username, email, created FROM person WHERE username = %s """ \
		,(username,))
		return DB.cursor.fetchone()

	def get_id_by_username(self, username):
		DB.cursor.execute(""" SELECT person_id FROM person WHERE username = %s """ \
		,(username,))
		return DB.cursor.fetchone()

	def login_by_username(self, username):
		DB.cursor.execute(""" SELECT person_id, username, email, created, password FROM person WHERE username = %s """ \
		,(username,))
		return DB.cursor.fetchone()

